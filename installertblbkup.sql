-- Table: public.InstallerBackup

-- DROP TABLE public."InstallerBackup";

CREATE TABLE public."InstallerBackup"
(
    "Auto_ID" integer NOT NULL DEFAULT nextval('"InstallerBackup_Auto_ID_seq"'::regclass),
    "ID" bigint NOT NULL,
    "CreateDate" timestamp without time zone NOT NULL,
    "UpdateDate" timestamp without time zone NOT NULL,
    "ManufacturerName" character varying(50) COLLATE pg_catalog."default" NOT NULL,
    "ProductModelName" character varying(50) COLLATE pg_catalog."default" NOT NULL,
    "Pair1" character varying(20) COLLATE pg_catalog."default",
    "Pair2" character varying(20) COLLATE pg_catalog."default",
    "Pair3" character varying(20) COLLATE pg_catalog."default",
    "Pair4" character varying(20) COLLATE pg_catalog."default",
    "Pair5" character varying(20) COLLATE pg_catalog."default",
    "Pair6" character varying(20) COLLATE pg_catalog."default",
    "Pair7" character varying(20) COLLATE pg_catalog."default",
    "Pair8" character varying(20) COLLATE pg_catalog."default",
    "Pair9" character varying(20) COLLATE pg_catalog."default",
    "Pair10" character varying(20) COLLATE pg_catalog."default",
    "Pair11" character varying(20) COLLATE pg_catalog."default",
    "Pair12" character varying(20) COLLATE pg_catalog."default",
    "Spare_Pin" character varying(20) COLLATE pg_catalog."default",
    "OtherCompatibleProductName" character varying(200) COLLATE pg_catalog."default",
    "RecommendProductName" character varying(200) COLLATE pg_catalog."default",
    "ProductCategory" character varying(100) COLLATE pg_catalog."default",
    "ProductSubCategory" character varying(100) COLLATE pg_catalog."default",
    "ModelImagePath" character varying(200) COLLATE pg_catalog."default",
    "Image" bytea,
    "ApplicationDetails" character varying(1000) COLLATE pg_catalog."default",
    "WiringAndBackPlate" character varying(500) COLLATE pg_catalog."default",
    "Instructions" character varying(1000) COLLATE pg_catalog."default",
    "IsNewInstall" character varying(20) COLLATE pg_catalog."default",
    "IsCommissioningProduct" character varying(10) COLLATE pg_catalog."default",
    "FAQURL" character varying(500) COLLATE pg_catalog."default",
    "VideoURL" character varying(500) COLLATE pg_catalog."default",
    "OtherUrl" character varying(500) COLLATE pg_catalog."default",
    "IsActive" boolean NOT NULL,
    "Version" integer,
    "PrivacyPolicyURL" character varying(500) COLLATE pg_catalog."default",
    "ContactDetails" character varying(1000) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE public."InstallerBackup"
    OWNER to postgres;