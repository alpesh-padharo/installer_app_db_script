-- Table: public.Logger

-- DROP TABLE public."Logger";

CREATE TABLE public."Logger"
(
    "ID" integer NOT NULL DEFAULT nextval('"Logger_ID_seq"'::regclass),
    "MethodName" character varying(100) COLLATE pg_catalog."default",
    "CreateDate" timestamp without time zone NOT NULL,
    "Exception" character varying(2000) COLLATE pg_catalog."default",
    "UpdateDate" timestamp without time zone,
    CONSTRAINT "LoggerTable_pkey" PRIMARY KEY ("ID")
)

TABLESPACE pg_default;

ALTER TABLE public."Logger"
    OWNER to postgres;